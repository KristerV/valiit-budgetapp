package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.management.StandardEmitterMBean;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

// https://gitlab.com/KristerV/valiit-budgetapp

public class Main extends Application {

    static ArrayList<String[]> andmebaas = new ArrayList<>(); // ei kasuta hetkel

    static VBox tabel = new VBox();
    static int koguSumma = 0;
    static Label summaLabel = new Label();

    @Override
    public void start(Stage stage) throws Exception{
        VBox vbox = new VBox();
        Scene scene = new Scene(vbox, 400, 600);
        stage.setScene(scene);
        stage.show();

        // Sektsioon ülemine
        summaLabel.setText("0€");
        summaLabel.setPadding(new Insets(64));
        summaLabel.setFont(new Font(32));
        StackPane summaPane = new StackPane();
        summaPane.getChildren().add(summaLabel);
        vbox.getChildren().add(summaPane);

        // Sektsioon keskmine
        HBox keskmineBox = new HBox();
        TextField kuluPealkiri = new TextField();
        kuluPealkiri.setPromptText("Kulu tüüp");
        TextField kuluSumma = new TextField();
        Button liida = new Button("+");
        Button lahuta = new Button("-");
        keskmineBox.getChildren().addAll(kuluPealkiri, kuluSumma, liida, lahuta);
        vbox.getChildren().add(keskmineBox);

        // Sektsioon alumine
        vbox.getChildren().add(tabel);

        // Andmete sisestamine
        lahuta.setOnMouseClicked(event -> {
            lisaAndmebaasi(kuluPealkiri.getText(), kuluSumma.getText(), true);
        });

        updateExpenseList();

    }

    public static void lisaAndmebaasi(String tyyp, String summa, boolean lahuta) {
        String sql = String.format("INSERT INTO budget (pealkiri, summa) VALUES ('%s', %s)", tyyp, summa);
        Andmebaas ab = new Andmebaas();
        ab.teostaAndmebaasiMuudatus(sql);
        ab.sulgeYhendus();
        updateExpenseList();
    }

    private static void updateExpenseList() {
        String sql = "SELECT pealkiri, summa FROM budget";
        Andmebaas ab = new Andmebaas();
        ResultSet rs = ab.get(sql);
        try {
            while (rs.next()) {
                Label rida = new Label(rs.getString("pealkiri") + " " + rs.getInt("summa"));
                tabel.getChildren().add(rida);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
